from django.contrib import admin
from .models import TodoList
from .models import TodoItem

# Register your models here.
class TodoListAdmin(admin.ModelAdmin):
    list_display = ('id','name')

admin.site.register(TodoList)

class TodoItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'task', 'list')

admin.site.register(TodoItem, TodoItemAdmin)
