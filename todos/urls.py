from django.urls import path
from todos.views import show_todos

urlpatterns = [
    path("todos/", show_todos),

]
