from django.shortcuts import render
from .models import TodoList


# Create your views here.
def show_todos(request):
    todo_lists = TodoList.objects.all()
    context = {'todo_lists':todo_lists}
    return render(request, 'todos/todos.html', context)
